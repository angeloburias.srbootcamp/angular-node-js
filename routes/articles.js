var express = require('express');
var router = express.Router();
const { articlesController } = require('../controller/articlesController');


router.get('/',async(request, response) => {
    const list = await articlesController.list();
    response.json(list);
});

router.post('/create',async (request, response) => {
  const body = request.body;
  const result = await articlesController.create(body);

    response.json(result);
});
 
router.put('/:id', async (request,response) => {
  const body = request.body;
  const params = request.body;
  const result = await articlesController.update(body,params.id);

  response.json({
    message: 'SUCCESS',
    result: result,
    id: params.id
  });

});

router.delete('/remove/:id',async(request,response) => {
  const params = request.params;

  await articlesController.delete(params.id);

  response.json({
    message: 'SUCCESS',
    id: params.id
  });
});


module.exports = router;
