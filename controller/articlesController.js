const db = require('../db/mysql');

db.connect();

queryHandler = (query)=> {
  return new Promise((resolve, reject) => {
    db.query(query, (err,result) => {
        err == true ? reject(err) : resolve(result);
    });
  });
}




module.exports.articlesController = {

  create(data){
    const query = `INSERT INTO posts(title, body, userId) 
    VALUES('${data.title}', '${data.body}', 1)`;
    queryHandler(query);
  },
  list(){
    const query = `SELECT * FROM posts`;
    return queryHandler(query);
  },
  update(data, id){
    const query = `UPDATE posts 
    SET title=${data.title}, 
    body=${data.body} 
    WHERE id=${id}`;

    queryHandler(query);
  },
  delete(id){
    const query = `DELETE FROM posts WHERE id=${id}`;
    queryHandler(query);
  }
}